# -*- coding: utf-8 -*-
import webapp2

import logging
import json
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import model.ModelArticle 

import jinja2
import os
import yql

from model.ModelArticle import ModelArticle
from random import shuffle
import random

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class MainPage(webapp2.RequestHandler):
    
    def get(self, key):
        article = db.get(key)
        content_len = len(article.content)
        
        category = article.category

        q = ModelArticle.all() 
        q.filter('category =', category)
        
        articles = q.fetch(50)
        shuffle(articles)
        related_news, related_news_length = [], 0

        for index, value in enumerate(articles):
            if index == 7:
                break
            if value.key() == key:
                continue
            related_news_length += len(value.get_title()) / 18
            if related_news_length > 8:
                break
            related_news += [{'link':str(value.key()),
                              'title':value.get_title()}]

        keywords = []
        for score, keyword in yql.analyze_content(article.content)['keywords']:
            keywords.append(keyword)

        style = random.randint(1, 2)
        ads = [
            'http://www.youtube.com/watch?v=_8W_Lnxn20A',
            'http://www.youtube.com/watch?v=RAbD3AGFX6I',
        ]
        word_limits = [360, 475]
        word_limit = max(word_limits[style - 1], content_len / 2)

        template_values = {
            'photo': None,
            'style': style,
            'title': article.get_title(),
            'content': {
                'top': [article.content[0:word_limit]],
                'bottom': [article.content[word_limit:]],
            },
            'related_news': related_news[0:7],
            'related_keyword': u'／'.join(keywords),
            'banner_ad': {
                'source': '/images/ad/AD_' + str(style) + '.jpg',
                'link': ads[style - 1]
            },
        }   

        path = os.path.join(os.path.dirname(__file__), '../template/template.html')
        self.response.out.write(template.render(path, template_values))

app = webapp2.WSGIApplication([('/read/yahoo/(.+)', MainPage)], debug=True)
