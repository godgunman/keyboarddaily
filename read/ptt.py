# -*- coding: utf-8 -*-
import webapp2

import logging
import json
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import jinja2
import os
import yql

from random import shuffle
from model.ModelPtt import ModelPtt
import random

import re
 
jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

def count_fullwidth_char(content, p):
    ref = re.findall('[a-zA-Z0-9_\!\.,\~+\-\*/\?=:\(\)]', content)
    return len(ref)


class MainPage(webapp2.RequestHandler):
    
    def get(self, key):

        ptt = db.get(key)
        content_len = len(ptt.content)
        content_index = content_len / 2 
    
        for i in range(content_index, content_len, 20):
            wl1 = count_fullwidth_char(ptt.content[0:i], self)
            word1 = wl1 + (i-wl1)*2
            
            wl2 = count_fullwidth_char(ptt.content[i:], self)
            word2 = wl2 + ((content_len-i)-wl2)*2
        
            if word1>word2+100:
                content_index = i
                break;
        
        comments = json.loads(ptt.comments)
        sel_comments = []
        for index, value in enumerate(comments):
            sel_comments += [{'floor':index,
                              'author':value['id'],
                              'content':value['content'],
                              }]
        shuffle(sel_comments)

        keywords = []
        for score, keyword in yql.analyze_content(ptt.content)['keywords']:
            keywords.append(keyword)

        style = random.randint(1, 2)

        template_values = {
            'photo': None,
            'style': style,
            'title': ptt.title,
            'content': {
                'top': [ptt.content[0:content_index]],
                'bottom': [ptt.content[content_index:]],
            },
            'related_keyword': u'／'.join(keywords),
            'related_comment': sel_comments[0:5],
            'banner_ad': 'images/ad/AD_' + str(style) + '.jpg',
        }   

        path = os.path.join(os.path.dirname(__file__), '../template/template.html')
        self.response.out.write(template.render(path, template_values))

app = webapp2.WSGIApplication([('/read/ptt/(.+)', MainPage)], debug=True)
