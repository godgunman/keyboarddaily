#!/usr/bin/env python

import json
import urllib

import webapp2
from google.appengine.api import urlfetch
from google.appengine.ext import db

import model.ModelArticle

import yql

""" Fetch the YQL Result """
class YQLAnalyzer(webapp2.RequestHandler):
    def get(self, key):
        self.response.headers['Content-Type'] = 'text/html'

        try:
            article = db.get(key)
            if article:
                title = str(article.key().name().encode('utf-8'))
            else:
                self.response.out.write('No such key: ' + key)
                return
        except Exception, e:
            self.response.out.write('Bad request')
            return

        if article:
            title = str(article.key().name().encode('utf-8'))
            self.response.out.write('<html><head>')
            self.response.out.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
            self.response.out.write('<title>' + title + '</title>')
            self.response.out.write('</head><body>')
            self.dump_query_result(article.content)
            self.response.out.write('</body></html>')
        else:
            self.response.out.write('No such key: ' + key)

    def dump_query_result(self, content):
        result = yql.analyze_content(content)
        keywords = result['keywords']
        categories = result['categories']

        if keywords != []:
            self.response.out.write('<h2>Keywords</h2>')
            self.response.out.write('<ul>')
            for keyword in keywords:
                self.response.out.write('<li>' + str(keyword[0]) + ' -- ' + keyword[1] + '</li>')
            self.response.out.write('</ul>')

        if categories != []:
            self.response.out.write('<h2>Categories</h2>')
            self.response.out.write('<ul>')
            for cat in categories:
                self.response.out.write('<li>' + str(cat[0]) + ' -- ' + cat[1] + '</li>')
            self.response.out.write('</ul>')

app = webapp2.WSGIApplication([
    ('/yql/analyze/(.+)', YQLAnalyzer)
], debug=True)
