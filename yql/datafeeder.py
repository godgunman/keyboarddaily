import webapp2
from google.appengine.api import urlfetch
from google.appengine.ext import db
import model.ModelArticle
import re

""" Data feeder for YQL Web spider """
class YQLDataFeeder(webapp2.RequestHandler):
    def get(self, key):
        self.response.headers['Content-Type'] = 'text/html'

        try:
            article = db.get(key)
            if article:
                title = str(article.key().name().encode('utf-8'))
                self.response.out.write('<html><head>')
                self.response.out.write('<title>' + title + '</title>')
                self.response.out.write('</head><body>')
                self.response.out.write('<h1>' + title + '</h1>')
                for paragraph in re.split('\n', article.content):
                    self.response.out.write('<p>' + paragraph + '</p>')
                self.response.out.write('</body></html>')
            else:
                self.response.out.write('No such key: ' + key)
        except Exception, e:
            self.response.out.write('Bad request')
            return

app = webapp2.WSGIApplication([
    ('/yql/datafeeder/(.+)', YQLDataFeeder)
], debug=True)
