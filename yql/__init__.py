#!/usr/bin/env python

import json, urllib
from google.appengine.api import urlfetch

_YQL_URL = "https://query.yahooapis.com/v1/public/yql";

def _request_analysis(content):
    dat = urllib.urlencode({
        'format': 'json',
        'q': 'select * from contentanalysis.analyze where text=@data;',
        'data': content.encode('utf-8')
    })

    response = urlfetch.fetch(url=_YQL_URL, payload=dat, method=urlfetch.POST)

    if response.status_code != 200:
        raise IOError('Bad YQL response ' + str(response.status_code))

    return json.loads(response.content)

def _norm_items(obj):
    if type(obj) is list:
        return obj
    elif type(obj) is dict:
        return [obj]
    else:
        return []

def _norm_analysis_result(results):
    keywords = []
    categories = []

    if 'entities' in results and 'entity' in results['entities']:
        for ent in _norm_items(results['entities']['entity']):
            try:
                keywords.append((ent['score'], ent['text']['content']))
            except KeyError, e:
                pass

    if 'yctCategories' in results and 'yctCategory' in results['yctCategories']:
        for ent in _norm_items(results['yctCategories']['yctCategory']):
            try:
                categories.append((ent['score'], ent['content']))
            except KeyError, e:
                pass

    return (keywords, categories)

def analyze_content(content):
    keywords = []
    categories = []

    try:
        response = _request_analysis(content)

        if 'query' not in response:
            raise ValueError('No query in response')

        response = response['query']

        if 'results' not in response:
            raise ValueError('No results in response')

        results = response['results']

        if results == None:
            raise ValueError('No results in response')

        keywords, categories = _norm_analysis_result(results)

    except IOError, e:
        pass
    except ValueError, e:
        pass

    return { 'keywords': keywords , 'categories': categories }
