import webapp2
from google.appengine.api import urlfetch

import logging
import json
from model import ModelArticle, ModelPtt
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from bs4 import BeautifulSoup

import jinja2
import os
import re, sys
from utility import model_convert

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

 
def escape(content):
    patt = re.compile(' ?\\[.*?m')
    return patt.sub('', content) 

class Manual(webapp2.RequestHandler):

    def post(self):

        self.response.headers['Content-Type'] = 'text/html'
        
        content = self.request.get('content')
        content = escape(content)
        lines = content.split('\n') 
        
        author = lines[0][lines[0].find(':')+1:lines[0].rfind(':')-2]
        category = lines[0][lines[0].rfind(':')+1:]

        title = lines[1][lines[1].find(':')+1:]
        datetime = lines[2][lines[2].find(':')+1:]
        
        del lines[0:3]

        content = ''
        comment_start = 0
        for index, value in enumerate(lines):
            content = content + value
            if value.find('(ptt.cc)') != -1:
                comment_start = index
                break
                
        lines = lines[index+2:]
        comments = []
        for index, value in enumerate(lines):
            
            type = value[0:value.find(' ')]
            value = value[value.find(' ')+1:]
            id = value[0:value.find(':')]
            value = value[value.find(':')+1:]
            content2 = value[:-12]
            date = value[-12:]
            comment = {'tpye':type.strip(),
                       'id':id.strip(),
                       'content':content2.strip(),
                       'date':date.strip()}
            comments += [comment]
                    
        k = Key.from_path('ModelPtt', abs(hash(datetime.strip()+author.strip())))
        ptt = db.get(k)

        if ptt == None:
            ptt = ModelPtt.ModelPtt(key=k)
            ptt.title = title.strip()
            ptt.author = author.strip()
            ptt.category = category.strip()
            ptt.content = content.strip()
            ptt.ptt_datetime = datetime.strip()
            ptt.comments = json.dumps(comments)
            ptt.put()

        self.response.out.write(model_convert.to_json(ptt))

    
    def get(self):
        template_values = { 
            'action':self.request.url,
            'method':'post',
            'text_fields':['content'],
        }   

        path = os.path.join(os.path.dirname(__file__),'../template/ptt_form.html')
        self.response.out.write(template.render(path,template_values))

app = webapp2.WSGIApplication([('/ptt/article/manual', Manual)], debug=True)
