# -*- coding: utf-8 -*-
import webapp2

import logging
import json
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import jinja2
import os

from random import shuffle
from model import ModelArticle
import random

import re
from utility.model_convert import to_json
 
jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

def count_fullwidth_char(content, p):
    ref = re.findall('[a-zA-Z0-9_\!\.,\~+\-\*/\?=:\(\)]', content)
    return len(ref)

def for_css(articles, magic):
    return {'first':articles[0:1],
            'second':articles[1:1+magic],
            'third':articles[1+magic:1+magic*2]}

class MainPage(webapp2.RequestHandler):
    
    def get(self):

        magic = 5

        life = ModelArticle.query_title_link_by_category('life',50)
        sports = ModelArticle.query_title_link_by_category('sports',50)
        society = ModelArticle.query_title_link_by_category('society',50)
        politics = ModelArticle.query_title_link_by_category('politics',50)
        tech = ModelArticle.query_title_link_by_category('tech',50)
        showbiz = ModelArticle.query_title_link_by_category('showbiz',50)
        realtime = ModelArticle.query_title_link_by_category('realtime',50)

        hot_news = for_css(politics, magic)
        biz_news = for_css(showbiz, magic)
        social_news = for_css(society, magic)
        life_news = for_css(life, magic)
        tech_news = for_css(tech, magic)
        sports_news = for_css(sports, magic)
        
        template_values = {
            'hot_news': hot_news,
            'biz_news': biz_news,
            'social_news': social_news,
            'life_news': life_news,
            'tech_news': tech_news,
            'sports_news': sports_news,
        }   

        path = os.path.join(os.path.dirname(__file__), 'template/hot.html')
        self.response.out.write(template.render(path, template_values))

app = webapp2.WSGIApplication([('/', MainPage)], debug=True)
