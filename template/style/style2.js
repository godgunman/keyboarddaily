$(function() {
    var rotate = function(name, font, className) {
        $(name).each(function(i) {
            if(className == undefined) {
                className = 'vt' + i;
                $(this).addClass(className);
            }
            (new Taketori())
                .set({ fontFamily: font })
                .element(name + '.' + className)
                .toVertical();
        });
    };

    var margin_right = ($('html').width() - $('body').width()) / 2;
    var align = function(name, offset) {
        $(name).each(function() {
            $(this)
                .css('position', 'absolute')
                .offset({
                    top: offset.top,
                    left: $('html').width() - (margin_right + offset.right + $(name).width())
                });
        });
    };

    $('header')
        .append(
            $('<div></div>')
                .addClass('hz-line')
                .css({
                    borderBottom: '2px solid rgba(127, 127, 127, 0.5)',
                    zIndex: -1
                })
                .width(1024)
        );

    align('header h1', { top: 20, right: 0 });
    align('header div.hz-line', { top: 74, right: 0 });
    align('section', { top: 120, right: 0 });

    $('<div></div>')
        .addClass('wrapper')
        .append($('section table'))
        .appendTo('section');

    rotate('div', 'serif', 'wrapper');
    align('section figure', { top: 170, right: 280 });
    align('div.ad', { top: 120, right: 944 });
});
