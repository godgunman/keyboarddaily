$(function() {
    var rotate = function(name, font, className, height) {
        (new Taketori())
            .set({ fontFamily: font, height: height + 'px' })
            .element(name + '.' + className)
            .toVertical();
    };

    var margin_right = ($('html').width() - $('body').width()) / 2;
    var align = function(name, offset) {
        $(name).each(function() {
            $(this)
                .css('position', 'absolute')
                .offset({
                    top: offset.top,
                    left: $('html').width() - (margin_right + offset.right + $(name).width())
                });
        });
    };

    $('header h1')
        .addClass('taketori-serif-ja-jp');

    $('section').each(function(i) {
        $('<div></div>')
            .addClass('wrapper' + i)
            .append($('ul', this))
            .appendTo($(this));
        rotate('div', 'serif', 'wrapper' + i, 180);
    });

    align('header h1', { top: 0, right: -20 });
    align('section.v1', { top: 74, right: 120 }); 
    align('section.v2', { top: 299, right: 120 });
    align('section.v3', { top: 524, right: 120 });
    align('section.v4', { top: 749, right: 120 });
    align('section.v5', { top: 974, right: 120 });
    align('section.v6', { top: 1199, right: 120 });

});
