$(function() {
    var rotate = function(name, font) {
        $(name).each(function() {
            $(this).addClass('vt');
        });
        (new Taketori())
            .set({ fontFamily: font })
            .element(name + '.vt')
            .toVertical();
    };
    rotate('section', 'serif');

    var margin_right = ($('html').width() - $('body').width()) / 2;
    var align = function(name, offset) {
        $(name).each(function() {
            $(this)
                .css('position', 'absolute')
                .offset({
                    top: offset.top,
                    left: $('html').width() - (margin_right + offset.right + $(name).width())
                });
        });
    };

    $('header')
        .append(
            $('<div></div>')
                .addClass('vt-line')
                .css({
                    borderLeft: '1px solid gray',
                    zIndex: -1
                })
                .height($('html').height())
        )
        .append(
            $('<div></div>')
                .addClass('hz-line')
                .css({
                    borderBottom: '2px solid rgba(127, 127, 127, 0.5)',
                    zIndex: -1
                })
                .width($('html').width() - 72)
        );

    align('header h1', { top: 0, right: 18 });
    align('header div.vt-line', { top: 0, right: 72 });
    align('header div.hz-line', { top: 74, right: 72 });
    align('section', { top: 100, right: 96 });
    align('div.ad', { top: 600, right: 96 });
});
