'''
Created on Oct 19, 2012

@author: ggm
'''
from model import BaseModel
from google.appengine.ext import db
import json

class ModelPtt(BaseModel.BaseModel):

    '''
    title is key_name
    '''
    title = db.StringProperty()
    author = db.StringProperty()
    content = db.TextProperty()
    comments = db.TextProperty()
    category = db.StringProperty()
    ptt_datetime = db.StringProperty()

    def __init__(self, *args, **kwargs):
        super(ModelPtt,self).__init__(*args, **kwargs)
        self.custom_add['comments'] = self.get_comments
                        
    def get_comments(self):      
        try:
            result = json.loads(self.comments, 'utf-8')
        except:                                 
            result = None
        return result    