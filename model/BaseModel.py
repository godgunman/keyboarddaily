'''
Created on 2011/8/26

@author: ggm
'''

from google.appengine.ext import db 
from utility import model_convert

class BaseModel(db.Model):

    datetime = db.DateTimeProperty(auto_now=True, auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(BaseModel, self).__init__(*args, **kwargs)
        self.custom_add={'base':self.get_base}
        self.custom_def={}

    def to_json(self):
        return model_convert.to_json(self)
    
    def to_dict(self):
        return model_convert.to_dict(self)
    
    def get_base(self):
        return {'id':str(self.key().id()),'key':str(self.key())}
    
