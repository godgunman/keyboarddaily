# -*- coding: utf-8 -*-
'''
Created on Oct 19, 2012

@author: ggm
'''
from model import BaseModel
from google.appengine.ext import db
from random import shuffle
import random

class ModelArticle(BaseModel.BaseModel):

    '''
    title is key_name
    '''
    
    link = db.TextProperty()
    source = db.TextProperty()
    content = db.TextProperty()
    category = db.StringProperty()
    article_datetime = db.StringProperty()
    
    def __init__(self, *args, **kwargs):
        super(ModelArticle,self).__init__(*args, **kwargs)
    
    def get_title(self):
        return self.key().name().replace(u' - Yahoo!奇摩新聞','')

def get_simple_article(article):

    return {'title':article.get_title()[0:15],
            'link':'read/yahoo/'+str(article.key())}

def query_title_link_by_category(category, limit=30):
    q = ModelArticle.all()
    q.order('datetime')
    q.filter('category =', category)
    articles = q.fetch(offset=random.randint(0,10), limit=limit)
    
    result = []
    shuffle(result)
    result = result[0:15]
    for article in articles:
        result += [get_simple_article(article)]
    
    return result
