'''
Created on Oct 19, 2012

@author: ggm
'''
from model import BaseModel
from google.appengine.ext import db

class ModelSummary(BaseModel.BaseModel):

    '''
    title is key_name
    '''
    
    link = db.TextProperty()
    source = db.TextProperty()
    category = db.StringProperty()
    description = db.TextProperty()
    has_atricle = db.BooleanProperty(default = False)
    