'''
Created on 2011/8/27

@author: ggm
'''
import datetime
import time
import logging
import json
from google.appengine.ext import db


SIMPLE_TYPES = (int, long, float, bool, dict, basestring, list)

def to_json(model):
    
    try:
        res = json.dumps(model)
    except:
        res = json.dumps(to_dict(model))
    return res

def to_dict(model):

    if isinstance(model,list):
        output = []
        for m in model:
            output += [to_dict(m)]
        return output

    elif model is None or isinstance(model, SIMPLE_TYPES):
        return model

    elif isinstance(model,db.Model):
        output = {}
        for key, prop in model.properties().iteritems():
            value = getattr(model, key)

            if hasattr(model,'custom_def') and key in model.custom_def:
                output[key] = to_dict(model.custom_def[key]())

            else:
                if value is None or isinstance(value, SIMPLE_TYPES):
                    output[key] = value
                elif isinstance(value, datetime.date):
                    # Convert date/datetime to ms-since-epoch ("new Date()").
        #            ms = time.mktime(value.utctimetuple()) * 1000
        #            ms += getattr(value, 'microseconds', 0) / 1000
                    output[key] = str(value)
                elif isinstance(value, db.GeoPt):
                    output[key] = {'lat': value.lat, 'lon': value.lon}
                elif isinstance(value, db.Model):
                    output[key] = to_dict(value)
                else:
                    raise ValueError('cannot encode ' + repr(prop))
    
        if hasattr(model,'custom_add'):
            for key, value in model.custom_add.items():
                output[key]=to_dict(value())
            
        return output
    
#TODO
def to_model(model,data):
    
    for key, prop in model.properties().iteritems():
        if key in data:
            setattr(model,key,data[key])
            
    return model