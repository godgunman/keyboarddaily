import webapp2
from google.appengine.api import urlfetch

import logging
import json
from model import ModelArticle
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from bs4 import BeautifulSoup

import jinja2
import os

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class Auto(webapp2.RequestHandler):
    
    def get(self):
        url = "http://tw.news.yahoo.com/rss/politics"
        result = urlfetch.fetch(url)

class Manual(webapp2.RequestHandler):

    def post(self):
#        url = "http://tw.news.yahoo.com/%E5%8E%9F%E6%B0%91%E5%AD%B8%E7%94%9F%E6%9C%AA%E7%B9%B3%E4%BD%9C%E6%A5%AD-%E8%80%81%E5%B8%AB%E7%AB%9F%E7%95%B6%E7%9C%BE%E6%8E%8C%E6%91%91-135259383.html"
        url = self.request.get('url')
        category = self.request.get('category')
        
        result = urlfetch.fetch(url)
        if result.status_code == 200:
            
            soup = BeautifulSoup(result.content.replace('<br>','').replace('</br>',''))

#            self.response.out.write(soup.body)
#            headline = soup.select("h1.headline")[0].string
#            self.response.out.write(headline)

            title = soup.title.string
            article_datetime = soup.select('abbr')[1]['title'] 

            bd = soup.select("div.yom-mod.yom-art-content")[0]
            p = bd.find_all('p')

            content = ''            
            for item in p:
                while item.span != None:
                    item.span.extract()
                while item.div != None:
                    item.div.extract()
                if item != None:
                    if not item.attrs :
                        line = str(item).replace('<p>','').replace('</p>','')
                        content += line 
            
            d = { 'title':title, 
                  'content':content, 
                  'article_datetime':article_datetime,
                  'category':category,
                }

            key = Key.from_path('ModelArticle', d['title'])
            article = db.get(key)
            
            if article == None:
                article = ModelArticle.ModelArticle(key_name=d['title'])
                article.content = content.decode('utf-8')
                article.article_datetime = article_datetime
                article.link = url
                article.category = category
                article.put()

            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write(json.dumps(d))                

    def get(self):
        template_values = { 
            'action':self.request.url,
            'method':'post',
            'text_fields':['url', 'category'],
        }   
            
        path = os.path.join(os.path.dirname(__file__),'../template/form.html')
        self.response.out.write(template.render(path,template_values))

class RemoveShortContent(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'

        # Dump every articles
        articles = ModelArticle.ModelArticle.all().fetch(limit=2000)
        self.response.out.write('<h1>Removed</h1>')
        self.response.out.write('<table border="1">')
        for article in articles:
            if article.content == None or len(article.content) < 50:
                title = article.key().name()
                content = '' if article.content == None else article.content
                key = str(article.key())

                self.response.out.write(u'<tr>')
                self.response.out.write(u'<td>' + title + u'</td>')
                self.response.out.write(u'<td>' + content + u'</td>')
                self.response.out.write(u'<td>' + key+ u'</td>')
                self.response.out.write(u'</tr>')

                db.delete(article)
        self.response.out.write('</table>')


app = webapp2.WSGIApplication([('/yahoo/article/auto', Auto),
                               ('/yahoo/article/manual', Manual),
                               ('/yahoo/article/remove_short', RemoveShortContent)], debug=True)
