import webapp2
from google.appengine.api import urlfetch

import xml.etree.ElementTree as ET
import logging
import json
from model import ModelSummary
from google.appengine.ext.db import Key
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import jinja2
import os

class RssParser(webapp2.RequestHandler):
    
    def post(self):
        category = self.request.get('category')

        if category == None or category == '':
            category = 'realtime'
        
        url = 'http://tw.news.yahoo.com/rss/' + category
        
        result = urlfetch.fetch(url)
        if result.status_code == 200:
            self.response.headers['Content-Type'] = 'text/plain'
            root = ET.fromstring(result.content)
            result = []
            for item in root.find('channel').findall('item'):
                
                d = {'title':item.find('title').text,
                     'description':item.find('description').text,
                     'source':item.find('source').get('url'),
                     'link':item.find('link').text,
                     'category':category,
                     }
                result += [d]
                result += [{'fetchurl':url}]
                
                key = Key.from_path('ModelSummary', d['title'])
                summary = db.get(key)
                
                if summary == None:
                    summary = ModelSummary.ModelSummary(key_name=d['title'])
                    summary.title = d['title']
                    summary.source = d['source']
                    summary.link = d['link']
                    summary.description = d['description']
                    summary.category = category
                    summary.put()

#            self.response.out.write(root[0][0].text)
            self.response.out.write(json.dumps(result))        
    
    def get(self):

        template_values = { 
            'action':self.request.url,
            'method':'post',
            'text_fields':['category'],
        }   
            
        path = os.path.join(os.path.dirname(__file__), '../template/form.html')
        self.response.out.write(template.render(path, template_values))



app = webapp2.WSGIApplication([('/yahoo/rss/manual', RssParser)], debug=True)
