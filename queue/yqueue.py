import webapp2
from google.appengine.api import urlfetch

import logging
from model import ModelSummary
from google.appengine.api import taskqueue

import json
import jinja2
import os

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class YArticlePush(webapp2.RequestHandler):

    def get(self):
        q = ModelSummary.ModelSummary.all()
        q.filter("has_atricle =", False)
        summaries = q.fetch(20)
        cnt = 0;
        for summary in summaries:
            taskqueue.add(url='/yahoo/article/manual', 
                          method='POST', 
                          params={'url': summary.link,
                                  'category': summary.category},
                          countdown=10)
            summary.has_atricle = True
            summary.put()
            cnt = cnt +1
        
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('cnt = '+str(cnt))

class YRssPush(webapp2.RequestHandler):
    def get(self):      
        categories = ['life', 'edu', 'sports', 'tech', 'taiwan', 'politics', 
                      'realtime', 'showbiz', 'society', 'intl', 'biz', 'health', 'art', 'travel']
        
        for category in categories:
            taskqueue.add(url='/yahoo/rss/manual', 
                          method='POST', 
                          params={'category': category})
            
        self.response.out.write(json.dumps(categories))

app = webapp2.WSGIApplication([('/queue/yahoo/article/push', YArticlePush),
                               ('/queue/yahoo/rss/push', YRssPush),], debug=True)
